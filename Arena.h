#ifndef ARENA_H_
#define ARENA_H_

typedef struct {
    char *start;
    ptrdiff_t size;
} Arena;

void arena_clean(Arena *a);
char *arena_end(Arena a);
void arena_rewind(Arena *a, char *ptr);

char *arena_alloc(Arena *a, size_t n);

#ifdef STRING_H_

char *arena_sv_to_cstr(Arena *a, String_View s);

#endif // STRING_H_

#endif // ARENA_H_

#ifdef ARENA_IMPLEMENTATION
#ifndef ARENA_C_
#define ARENA_C_

inline void arena_clean(Arena *arena)
{
    arena->size = 0;
}

inline char *arena_end(Arena arena)
{
    return arena.start + arena.size;
}

inline void arena_rewind(Arena *arena, char *ptr)
{
    assert(ptr >= arena->start);
    arena->size = ptr - arena->start;
}

inline char *arena_alloc(Arena *arena, size_t n)
{
    char *result = arena_end(*arena);
    memset(result, 0, n);
    arena->size += n;
    return result;
}

#ifdef STRING_H_
inline char *arena_sv_to_cstr(Arena *arena, String_View s)
{
    char *result = arena_alloc(arena, s.count + 1);
    memcpy(result, s.data, s.count);
    return result;
}
#endif // STRING_H_

#endif // ARENA_C_
#endif // ARENA_IMPLEMENTATION