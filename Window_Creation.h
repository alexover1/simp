#ifndef WINDOW_CREATION_H_
#define WINDOW_CREATION_H_

#include <SDL2/SDL.h>
#include <assert.h>

#include "Basic.h"

typedef SDL_Window *Window_Internal;
typedef SDL_GLContext Window_Context;
typedef SDL_Event Window_Event;

typedef struct {
    Window_Internal internal;
    Window_Context context;
    Window_Event *events;

    size_t width;
    size_t height;
} Window;

void simple_window_init();

Window create_window(const char *title, size_t width, size_t height);
void destroy_window(Window window);
void update_window_events(Window window);

#endif // WINDOW_CREATION_H_

#ifdef WINDOW_CREATION_IMPLEMENTATION

#ifndef WINDOW_CREATION_C_
#define WINDOW_CREATION_C_

void simple_window_init()
{
    assert(SDL_Init(SDL_INIT_VIDEO) >= 0);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //glEnable(GL_DEBUG_OUTPUT);
    //glDebugMessageCallback(MessageCallback, 0);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
}

Window create_window(const char *title, size_t width, size_t height)
{
    Window window;
    window.width = width;
    window.height = height;

    window.internal = SDL_CreateWindow(title,
                                SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                width, height,
                                SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    assert(window.internal != NULL);
    window.context = SDL_GL_CreateContext(window.internal);
    assert(window.context != NULL);
    return window;
}

void destroy_window(Window window)
{
    SDL_GL_DeleteContext(window.context);
    SDL_DestroyWindow(window.internal);
}

void update_window_events(Window window)
{
    arrfree(window.events);
    window.events = NULL;

    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        arrput(window.events, event);
    }
}

void update_window(Window window)
{
    SDL_GL_SwapWindow(window.internal);
}

#endif // WINDOW_CREATION_C_
#endif // WINDOW_CREATION_IMPLEMENTATION
