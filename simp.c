#define FILE_IMPLEMENTATION
#include "File.h"

#define LEXER_IMPLEMENTATION
#include "Lexer.h"

#define WINDOW_CREATION_IMPLEMENTATION
#include "Window_Creation.h"

#define ARENA_IMPLEMENTATION
#include "Arena.h"

#define STRING_IMPLEMENTATION
#include "String.h"

#define BASIC_IMPLEMENTATION
#include "Basic.h"