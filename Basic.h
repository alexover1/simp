#ifndef BASIC_H_
#define BASIC_H_

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <errno.h>
#include <assert.h>

#include "stb_ds.h"

#define UNREACHABLE assert(0 && "unreachable")
#define UNUSED(x) (void)(x)
#define UNIMPLEMENTED(x) assert(0 && "not implemented yet: "x)

void logprint(char *fmt, ...);

// TODO: Builder

#endif // BASIC_H_

#ifdef BASIC_IMPLEMENTATION

#ifndef BASIC_C_
#define BASIC_C_

#include <stdarg.h>

#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"

void logprint(char *fmt, ...)
{
    va_list argp;
    va_start(argp, fmt);
    vprintf(fmt, argp);
    va_end(argp);
}

#endif // BASIC_C_
#endif // BASIC_IMPLEMENTATION