#ifndef FILE_H_
#define FILE_H_

#include "String.h"
#include "Basic.h"

typedef int Errno;

#define return_defer(value) do { result = (value); goto defer; } while (0)
#define return_defer_if(err)   \
    do {                       \
        if (err != 0) {        \
            return_defer(err); \
        }                      \
    } while (0)                \

Errno read_entire_file(Cstr filename, char **buffer, size_t *buffer_size);
Errno write_entire_file(Cstr filename, char *buffer, size_t buffer_size);

#endif // FILE_H_

#ifdef FILE_IMPLEMENTATION

#ifndef FILE_C_
#define FILE_C_

Errno read_entire_file(Cstr filename, char **buffer, size_t *buffer_size)
{
    Errno result = 0;
    FILE *f = NULL;

    f = fopen(filename, "rb");
    if (f == NULL) return_defer(errno);

    if (fseek(f, 0, SEEK_END) < 0) return_defer(errno);
    long m = ftell(f);
    if (m < 0) return_defer(errno);
    if (fseek(f, 0, SEEK_SET) < 0) return_defer(errno);

    *buffer_size = m;
    *buffer = malloc(*buffer_size + 1);
    assert(*buffer);
    (*buffer)[*buffer_size] = '\0';

    fread(*buffer, *buffer_size, 1, f);
    if (ferror(f)) return_defer(errno);

defer:
    if (result != 0) {
        logprint("Could not read file '%s': %s", filename, strerror(result));
    }
    if (f) fclose(f);
    return result;
}

Errno write_entire_file(Cstr filename, char *buffer, size_t buffer_size) {
    Errno result = 0;
    FILE *f = NULL;

    f = fopen(filename, "w");
    if (f == NULL) return_defer(errno);

    size_t m = fwrite(buffer, 1, buffer_size, f);
    if (m != buffer_size) return_defer(errno);

defer:
    if (result != 0) {
        logprint("Could not write file '%s': %s", filename, strerror(result));
    }
    if (f) fclose(f);
    return result;
}

#endif // FILE_C_
#endif // FILE_IMPLEMENTATION
