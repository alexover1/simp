#ifndef SIMPLE_RENDERER_H_
#define SIMPLE_RENDERER_H_

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <assert.h>

#define GL_GLEXT_PROTOTYPES
#include <SDL2/SDL_opengl.h>

#include "la.h"

typedef enum {
    SHADER_FOR_COLOR = 0,
    COUNT_SIMPLE_SHADERS,
} Simple_Shader;

typedef struct {
    Vec2f position;
    Vec4f color;
    Vec2f uv;
} Simple_Vertex;

typedef enum {
    SIMPLE_VERTEX_ATTR_POSITION = 0,
    SIMPLE_VERTEX_ATTR_COLOR,
    SIMPLE_VERTEX_ATTR_UV,
} Simple_Vertex_Attr;

#ifndef SIMPLE_VERTICES_CAP
#define SIMPLE_VERTICES_CAP (3*640*1000)
#endif

typedef struct {
    GLuint vao;
    GLuint vbo;
    GLuint programs[COUNT_SIMPLE_SHADERS];
    Simple_Shader current_shader;

    Simple_Vertex vertices[SIMPLE_VERTICES_CAP];
    size_t vertices_count;
} Simple_Renderer;

void simple_renderer_init(Simple_Renderer *sr,
                          const char *vert_file_path);

void simple_renderer_vertex(Simple_Renderer *sr,
                            Vec2f p, Vec4f c, Vec2f uv);
void simple_renderer_triangle(Simple_Renderer *sr,
                              Vec2f p0, Vec2f p1, Vec2f p2,
                              Vec4f c0, Vec4f c1, Vec4f c2,
                              Vec2f uv0, Vec2f uv1, Vec2f uv2);
void simple_renderer_quad(Simple_Renderer *sr,
                          Vec2f p0, Vec2f p1, Vec2f p2, Vec2f p3,
                          Vec4f c0, Vec4f c1, Vec4f c2, Vec4f c3,
                          Vec2f uv0, Vec2f uv1, Vec2f uv2, Vec2f uv3);
void simple_renderer_sync(Simple_Renderer *sr);
void simple_renderer_draw(Simple_Renderer *sr);
void simple_renderer_solid_rect(Simple_Renderer *sr, Vec2f p, Vec2f s, Vec4f c);

void simple_renderer_flush(Simple_Renderer *sr);
void simple_renderer_set_shader(Simple_Renderer *sr, Simple_Shader shader);

#endif // SIMPLE_RENDERER_H_

#ifdef SIMPLE_RENDERER_IMPLEMENTATION

const char *frag_shader_file_paths[COUNT_SIMPLE_SHADERS] = {
    [SHADER_FOR_COLOR] = "./shaders/simple_color.frag",
};

static const char *shader_type_as_cstr(GLuint shader)
{
    switch (shader) {
    case GL_VERTEX_SHADER:
        return "GL_VERTEX_SHADER";
    case GL_FRAGMENT_SHADER:
        return "GL_FRAGMENT_SHADER";
    default:
        return "(Unknown)";
    }
}

static bool compile_shader_source(const GLchar *source, GLenum shader_type, GLuint *shader)
{
    *shader = glCreateShader(shader_type);

    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);

    GLint compiled = 0;
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &compiled);

    if (!compiled) {
        GLchar message[1024];
        GLsizei message_size = 0;
        glGetShaderInfoLog(*shader, sizeof(message), &message_size, message);
        fprintf(stderr, "Error: could not compile %s\n", shader_type_as_cstr(shader_type));
        fprintf(stderr, "%.*s\n", message_size, message);
        return false;
    }

    return true;
}

static bool compile_shader_file(const char *file_path, GLenum shader_type, GLuint *shader)
{
    bool result = true;

    char *buffer;
    size_t buffer_size;
    if (read_entire_file(file_path, &buffer, &buffer_size) != 0) {
        fprintf(stderr, "Error: failed to read `%s` shader file\n", file_path);
        return false; // read_entire_file frees if it errors.
    }

    if (!compile_shader_source(buffer, shader_type, shader)) {
        fprintf(stderr, "Error: failed to compile `%s` shader file\n", file_path);
        return_defer(false);
    }

defer:
    if (buffer != NULL) free(buffer);
    return result;
}

static void attach_shaders_to_program(GLuint *shaders, size_t shaders_count, GLuint program)
{
    for (size_t i = 0; i < shaders_count; ++i) {
        glAttachShader(program, shaders[i]);
    }
}

static bool link_program(GLuint program, const char *file_path, size_t line)
{
    glLinkProgram(program);

    GLint linked = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (!linked) {
        GLsizei message_size = 0;
        GLchar message[1024];

        glGetProgramInfoLog(program, sizeof(message), &message_size, message);
        fprintf(stderr, "%s:%zu: Program Linking: %.*s\n", file_path, line, message_size, message);
    }

    return linked;
}

void simple_renderer_init(Simple_Renderer *sr, const char *vert_file_path)
{
    glGenVertexArrays(1, &sr->vao);
    glBindVertexArray(sr->vao);

    glGenBuffers(1, &sr->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, sr->vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sr->vertices), sr->vertices, GL_DYNAMIC_DRAW);

    // position
    glEnableVertexAttribArray(SIMPLE_VERTEX_ATTR_POSITION);
    glVertexAttribPointer(
        SIMPLE_VERTEX_ATTR_POSITION,
        2,
        GL_FLOAT,
        GL_FALSE,
        sizeof(Simple_Vertex),
        (GLvoid *) offsetof(Simple_Vertex, position));

    // color
    glEnableVertexAttribArray(SIMPLE_VERTEX_ATTR_COLOR);
    glVertexAttribPointer(
        SIMPLE_VERTEX_ATTR_COLOR,
        4,
        GL_FLOAT,
        GL_FALSE,
        sizeof(Simple_Vertex),
        (GLvoid *) offsetof(Simple_Vertex, color));

    // uv
    glEnableVertexAttribArray(SIMPLE_VERTEX_ATTR_UV);
    glVertexAttribPointer(
        SIMPLE_VERTEX_ATTR_UV,
        2,
        GL_FLOAT,
        GL_FALSE,
        sizeof(Simple_Vertex),
        (GLvoid *) offsetof(Simple_Vertex, uv));

    GLuint shaders[2] = {0};

    if (!compile_shader_file(vert_file_path, GL_VERTEX_SHADER, &shaders[0])) {
        exit(1);
    }

    for (int i = 0; i < COUNT_SIMPLE_SHADERS; ++i) {
        if (!compile_shader_file(frag_shader_file_paths[i], GL_FRAGMENT_SHADER, &shaders[1])) {
            exit(1);
        }
        sr->programs[i] = glCreateProgram();
        attach_shaders_to_program(shaders, sizeof(shaders) / sizeof(shaders[0]), sr->programs[i]);

        if (!link_program(sr->programs[i], __FILE__, __LINE__)) {
            exit(1);
        }
    }
}

void simple_renderer_vertex(Simple_Renderer *sr,
                            Vec2f p, Vec4f c, Vec2f uv)
{
    assert(sr->vertices_count < SIMPLE_VERTICES_CAP);
    Simple_Vertex *last = &sr->vertices[sr->vertices_count];
    last->position = p;
    last->color    = c;
    last->uv       = uv;
    sr->vertices_count += 1;
}

void simple_renderer_triangle(Simple_Renderer *sr,
                              Vec2f p0, Vec2f p1, Vec2f p2,
                              Vec4f c0, Vec4f c1, Vec4f c2,
                              Vec2f uv0, Vec2f uv1, Vec2f uv2)
{
    simple_renderer_vertex(sr, p0, c0, uv0);
    simple_renderer_vertex(sr, p1, c1, uv1);
    simple_renderer_vertex(sr, p2, c2, uv2);
}

void simple_renderer_quad(Simple_Renderer *sr,
                          Vec2f p0, Vec2f p1, Vec2f p2, Vec2f p3,
                          Vec4f c0, Vec4f c1, Vec4f c2, Vec4f c3,
                          Vec2f uv0, Vec2f uv1, Vec2f uv2, Vec2f uv3)
{
    simple_renderer_triangle(sr, p0, p1, p2, c0, c1, c2, uv0, uv1, uv2);
    simple_renderer_triangle(sr, p1, p2, p3, c1, c2, c3, uv1, uv2, uv3);
}

void simple_renderer_solid_rect(Simple_Renderer *sr, Vec2f p, Vec2f s, Vec4f c)
{
    Vec2f uv = vec2fs(0);
    simple_renderer_quad(
        sr,
        p, vec2f_add(p, vec2f(s.x, 0)), vec2f_add(p, vec2f(0, s.y)), vec2f_add(p, s),
        c, c, c, c,
        uv, uv, uv, uv);
}

void simple_renderer_sync(Simple_Renderer *sr)
{
    glBufferSubData(GL_ARRAY_BUFFER,
                    0,
                    sr->vertices_count * sizeof(Simple_Vertex),
                    sr->vertices);
}

void simple_renderer_draw(Simple_Renderer *sr)
{
    glDrawArrays(GL_TRIANGLES, 0, sr->vertices_count);
}

void simple_renderer_flush(Simple_Renderer *sr)
{
    simple_renderer_sync(sr);
    simple_renderer_draw(sr);
    sr->vertices_count = 0;
}

void simple_renderer_set_shader(Simple_Renderer *sr, Simple_Shader shader)
{
    if (sr->current_shader == shader) return;

    simple_renderer_flush(sr);
    sr->current_shader = shader;
    glUseProgram(sr->programs[sr->current_shader]);
}

#endif // SIMPLE_RENDERER_IMPLEMENTATION
