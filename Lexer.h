#ifndef LEXER_H_
#define LEXER_H_

#include "String.h"

extern String_View DELIMITERS;
bool parse_token(String_View *file, String_View *out_token);

#endif // LEXER_H_

#ifdef LEXER_IMPLEMENTATION

#ifndef LEXER_C_
#define LEXER_C_

#include <stddef.h>
#include <string.h>
#include <stdbool.h>

bool parse_token(String_View *file, String_View *out_token)
{
    // chop whitespace
    *file = sv_trim_left(*file);
    if (file->count == 0) {
        return false;
    }
    // check if single-character delim
    for (size_t i = 0; i < DELIMITERS.count; ++i) {
        if (file->data[0] == DELIMITERS.data[i]) {
            if (out_token) *out_token = sv_chop_left(file, 1);
            return true;
        }
    }
    // next token until space or delim
    size_t n = 0;
    while (n < file->count && !isspace(file->data[n])) {
        // delim
        for (size_t i = 0; i < DELIMITERS.count; ++i) {
            if (file->data[n] == DELIMITERS.data[i]) {
                goto out;
            }
        }
        n += 1;
    }
out:
    if (out_token) *out_token = sv_chop_left(file, n);
    return true;
}


#endif // LEXER_C_
#endif // LEXER_IMPLEMENTATION